# README

Todo:
- [x] EM oblique incidence
- [x] Diffraction
- [ ] Aliasing
- [x] DFT simulation
- [ ] Main page
- [x] Polarization

---

This is a simulation project for classes MSAE-E4206 and MSAE-E4215, i.e., *E&M* and *Mechanical behavior*, so I name it "EMMB"rook.

## Contact

Please contact [qz2280@columbia.edu](mailto:qz2280@columbia.edu) if you are still looking for help. Thanks!
